package model;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Set;

public class Dictionar implements DictionarProc,Serializable
{
	private static final long serialVersionUID = 1L;
	private HashMap<String,DetaliiCuvant> myHash;

	public Dictionar()
	{
		myHash=new HashMap<String,DetaliiCuvant>();
	}

	public void adaugaCuvant(String denumire,DetaliiCuvant info) 
	{
		assert !cautaCuvant(denumire)==true:"Cuvantul deja face parte din vocabular";
		assert info!=null:"Eroare la adaugarea cuvantului!!";
		int size=myHash.size();
		
		assert isWellFormed():"Dictionarul nu este consistent(inainte de adaugare)!!";
		myHash.put(denumire,info);
		actualizeazaSinonimeleDupaAdaugare(denumire,myHash.get(denumire).getListaSinonime());
		assert isWellFormed():"Dictionarul nu este consistent(dupa actualizare)!!";
		int newSize=myHash.size();
		assert newSize==size+1:"Cuvantul nu a fost adaugat";
	}
	
	public void stergeCuvant(String cuvant,DetaliiCuvant info)
	{
		assert cautaCuvant(cuvant)==true:"Eroare la stergere";
		assert cuvant!=null:"Eroare la stergere!!";
		assert isWellFormed():"Dictionarul nu este consistent(inainte de stergere)!!";
		int oldSize=myHash.size();
		myHash.remove(cuvant);
		actualizeazaSinonimeleDupaStergere(cuvant,info.getListaSinonime());
		assert isWellFormed():"Dictionarul nu este consistent(dupa stergere)!!";
		int newSize=myHash.size();
		assert newSize==oldSize-1:"Eroare la Stergere!!";
	}
	
	public void adaugaSinonim(String denumire,String sinonim)
	{
		assert cautaCuvant(denumire)==true:"Eroare la adaugarea sinonimului";
		assert isWellFormed():"Dictionarul nu este consistent(dupa actualizare)!!";
		myHash.get(denumire).setListaSinonime(sinonim);
		actualizeazaSinonimeleDupaAdaugare(denumire,sinonim);
		assert isWellFormed():"Dictionarul nu este consistent(dupa actualizare)!!";
	}
	
	public void actualizeazaSinonimeleDupaAdaugare(String denumireCuvant,String sinonime)
	{
		ArrayList<String> sinonimele=new ArrayList<String>();
		sinonimele=convertStringSinonime(sinonime);
		
		for(String s:sinonimele)
		{
			for(String sor:returneazaCuvinteleSortate())
			{
				if(s.equals(sor)==true && cautaInformatiiCuvant(sor).getListaSinonime().contains(denumireCuvant)==false)
				{
					if(myHash.get(sor).getListaSinonime().length()==0)
					{
						myHash.get(sor).setListaSinonime(denumireCuvant);
					}
					else myHash.get(sor).setListaSinonime(myHash.get(sor).getListaSinonime()+","+denumireCuvant);
				}
			}
		}
	}
	
	public void actualizeazaSinonimeleDupaStergere(String denumire,String sinonime)
	{
		for(String sin:convertStringSinonime(sinonime))
		{
			if(myHash.get(sin).getListaSinonime().contains(denumire)==true)
			{
				myHash.get(sin).setListaSinonime(myHash.get(sin).getListaSinonime().replace(denumire, ""));
			}
		}
	}

	public boolean cautaCuvant(String denumire)
	{
		assert denumire!=null:"Eroare la cautare";
		for(String s:returneazaCuvinteleSortate())
		{
			if(s.equals(denumire))
			{
				return true;
			}
		}
		return false;
	}
	
	public ArrayList<String> searchForStartWith(String prefix)
	{
		ArrayList<String> l=new ArrayList<String>();
		for(String s:returneazaCuvinteleSortate())
		{
			if(s.startsWith(prefix)==true)
			{
				l.add(s);
			}
		}return l;
	}

	public ArrayList<String> searchStopWith(String prefix)
	{
		ArrayList<String> l=new ArrayList<String>();
			for(String s:returneazaCuvinteleSortate())
			{
				if(s.endsWith(prefix)==true)
				{
					l.add(s);
				}
			}return l;
	}
	
	public ArrayList<String> searchExpression(String prefix)
	{
		ArrayList<String> l=new ArrayList<String>();
			for(String s:returneazaCuvinteleSortate())
			{
				if(s.endsWith(prefix)==true)
				{
					l.add(s);
				}
			}return l;
	}
	
	public ArrayList<String> returneazaCuvinteFavorite()
	{
		ArrayList<String> favorite=new ArrayList<String>();
		for(String s:returneazaCuvinteleSortate())
		{
			if(myHash.get(s).getFavorite().equals("DA")==true)
			{
				favorite.add(s);
			}
		}
		return favorite;
	}
	
	public ArrayList<String> returneazaCuvinteleSortate()
	{
		ArrayList<String> listaCuvinte=new ArrayList<String>();
		Set<String> mySet=myHash.keySet();
		for(String s:mySet)
		{
			listaCuvinte.add(s.toString());
		}
		Collections.sort(listaCuvinte);
		return listaCuvinte;
	}
	
	public ArrayList<String> convertStringSinonime(String sinonime)
	{
		ArrayList<String> ss=new ArrayList<String>(Arrays.asList(sinonime.split(",")));
		return ss;
	}
	
	public DetaliiCuvant cautaInformatiiCuvant(String denumire)
	{
		assert cautaCuvant(denumire)==true:"Eroare la cautare";
		DetaliiCuvant info=new DetaliiCuvant();
		info.setExplicatie(myHash.get(denumire).getExplicatie());
		info.setListaSinonime(myHash.get(denumire).getListaSinonime());
		info.setTip(myHash.get(denumire).getTip());
		
		assert info!=null:"Eroare la cautare";
		return info;
	}

	public HashMap<String,DetaliiCuvant> getCUVINTE()
	{
		return this.myHash;
	}

	public boolean matches(String sourceWord, String searchedWord) {
		String pattern = searchedWord.replace('?', '.').replace("*","[\\s\\S]*");
		return sourceWord.matches(pattern);
	}

	public boolean isWellFormed()
	{
		for(String s:returneazaCuvinteleSortate())
		{
			for(String ss:convertStringSinonime(myHash.get(s).getListaSinonime()))
			{
				if(ss.equals("")==false  && cautaCuvant(ss)==false )
				{
						return false;
				}
				if(ss.equals("")==false && myHash.get(ss).getListaSinonime().contains(s)==false)
				{
					return false;
				}
			}
		}
		return true;
	}
}
