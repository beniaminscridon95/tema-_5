package model;
import java.io.Serializable;

public class DetaliiCuvant implements Serializable
{
	private static final long serialVersionUID = 1L;
	private String tip; 
	private String explicatie;
	private String listaSinonime;
	private String favorite;
	
	public DetaliiCuvant(){}

	public String getTip() 
	{
		return tip;
	}

	public void setTip(String tip) 
	{
		this.tip = tip;
	}

	public String getExplicatie() 
	{
		return explicatie;
	}

	public void setExplicatie(String explicatie) 
	{
		
		this.explicatie = explicatie;
	}

	public String getListaSinonime() 
	{
		return listaSinonime;
	}
	
	public void setListaSinonime(String listaSinonime) 
	{
		this.listaSinonime = listaSinonime;
	}

	public String getFavorite() 
	{
		return favorite;
	}

	public void setFavorite(String favorite) 
	{
		this.favorite = favorite;
	}
	
}
