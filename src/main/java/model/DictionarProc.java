package model;

public interface DictionarProc 
{
	/*
	 invariant-consistenta dictionar(pentru sinonime);
	 */
	public void adaugaCuvant(String denumire,DetaliiCuvant info);
	public void stergeCuvant(String cuvant,DetaliiCuvant info);
	public void adaugaSinonim(String denumire,String sinonim);
}
