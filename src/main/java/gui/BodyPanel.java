package gui;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.LineBorder;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import model.DetaliiCuvant;
import model.Dictionar;


public class BodyPanel extends JPanel 
{
	private static final long serialVersionUID = 1L;
	private Dictionar DICTIONAR;
	private boolean beeb=true;
	private JTable table;
	public String radioB;
	private JScrollPane scrollPane;
	private JRadioButton rdbtnStartWith;
	private JRadioButton rdbtnSfarseste;
	private JRadioButton rdbtnExpresie;
	
	
	public BodyPanel(Dictionar d) 
	{
		DICTIONAR=d;
		setBounds(0, 87, 959, 420);
		setBackground(new Color(225,226,227));
		//setLayout(null);
		setLayout(new CardLayout(0, 0));
		add(adaugaPanelWelcome(),"welcome");
	}

	protected  JPanel adaugaPanelWelcome()
	{
		JPanel panelWelcome = new JPanel();
		panelWelcome.setBounds(0, 0, 959, 420);
		
		JLabel label_1 = new JLabel("");
		panelWelcome.add(label_1, "name_19419321647240");
		label_1.setFont(new Font("Tahoma", Font.PLAIN, 30));
		label_1.setIcon(new ImageIcon("src\\main\\resources\\words.jpg"));
		
		return panelWelcome;
	}

	protected JPanel vizualizeazaCuvintele(TableModel model)
	{
		JPanel p=new JPanel();
		p.setBackground(new Color(225,226,227));
		p.setLayout(null);
		
		if(DICTIONAR.getCUVINTE().size()==0)
		{
			final JTextArea tAreaMessaje = new JTextArea("Va rugam importati cuvinte dintr-un fisier,\n"+"         apasand pe butonul 'Resurse'!!");
			tAreaMessaje.setForeground(new Color(17,78,96));
			tAreaMessaje.setBackground(new Color(225,226,227));
			tAreaMessaje.setFont(new Font("Tempus Sans ITC", Font.BOLD, 30));
			tAreaMessaje.setBounds(180, 147, 650, 350);
			p.add(tAreaMessaje);
		
		}
		else
		{
			 scrollPane = new JScrollPane();
			scrollPane.setBounds(0, 0, 959, 434);
			scrollPane.getViewport().setBackground(new Color(225,226,227));
			p.add(scrollPane);
			scrollPane.getVerticalScrollBar().setBackground(new Color(17,78,96));
		
			table = new JTable();
			table.setModel(model);
			table.setFont(new Font("Trebuchet MS", Font.PLAIN, 24));
			table.setRowHeight(45);
			table.setForeground(new Color(70, 70, 70));
			table.setBackground(new Color(225,226,227));
			table.getTableHeader().setPreferredSize(new Dimension(0,0));
			DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
			rightRenderer.setHorizontalAlignment(DefaultTableCellRenderer.CENTER);
			table.getColumn("").setCellRenderer( rightRenderer );
			scrollPane.setViewportView(table);
		}
		return p;
	}
	
	protected TableModel addJTableDenumireCuvinte()
	{
		DefaultTableModel model=new DefaultTableModel();
		model.addColumn("");
		if(DICTIONAR!=null)
		{
			ArrayList<String> ss=DICTIONAR.returneazaCuvinteleSortate();
			for(int i=0; i< ss.size(); i++)
			{
				model.addRow(new Object[]{ss.get(i)});
			}
		}
		return model;
	}

	protected JTable getTable()
	{
		return table;
	}

	@SuppressWarnings("unchecked")
	protected JPanel adaugaCuvantPanel()
	{
		final JPanel p=new JPanel();
		p.setLayout(null);
		p.setBackground(new Color(225,226,227));
	
		JLabel lblDenumire = new JLabel("Denumire");
		lblDenumire.setForeground(new Color(17,78,96));
		lblDenumire.setFont(new Font("Tempus Sans ITC", Font.BOLD, 22));
		lblDenumire.setBounds(184, 28, 100, 26);
		p.add(lblDenumire);
		
		JLabel lblExplicatie = new JLabel("Explicatie");
		lblExplicatie.setForeground(new Color(17,78,96));
		lblExplicatie.setFont(new Font("Tempus Sans ITC", Font.BOLD, 22));
		lblExplicatie.setBounds(184, 122, 100, 26);
		p.add(lblExplicatie);
		
		JLabel lblSinonime = new JLabel("Sinonime");
		lblSinonime.setForeground(new Color(17,78,96));
		lblSinonime.setFont(new Font("Tempus Sans ITC", Font.BOLD, 22));
		lblSinonime.setBounds(184, 277, 100, 26);
		p.add(lblSinonime);
		
		final JTextField tDenumire = new JTextField();
		tDenumire.setBackground(new Color(200,200,200));
		tDenumire.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
			char c = e.getKeyChar();
					if(Character.isDigit(c))
					{
						e.consume();
				}}});
		tDenumire.setFont(new Font("Constantia", Font.PLAIN, 22));
		tDenumire.setBounds(297, 27, 454, 30);
		p.add(tDenumire);
		tDenumire.setColumns(10);
		
		final JTextArea tAreaExplicatie = new JTextArea();
		tAreaExplicatie.setBackground(new Color(200,200,200));
		tAreaExplicatie.setFont(new Font("Constantia", Font.PLAIN, 21));
		tAreaExplicatie.setBounds(297, 121, 454, 139);
		p.add(tAreaExplicatie);
		
		JLabel lblTip = new JLabel("Tip");
		lblTip.setForeground(new Color(17,78,96));
		lblTip.setFont(new Font("Tempus Sans ITC", Font.BOLD, 22));
		lblTip.setBounds(245, 75, 39, 26);
		p.add(lblTip);
		
		String[] tip=new String[]{"substantiv","adjectiv","verb","adverb","articol hotarat",
								  "articol nehotarat","conjunctie","prepozitie"};
		
		@SuppressWarnings("rawtypes")
		final JComboBox comboBox = new JComboBox(tip);
		comboBox.setBackground(new Color(200,200,200));
		comboBox.setFont(new Font("Constantia", Font.PLAIN, 20));
		comboBox.setBounds(297, 75, 199, 30);
		p.add(comboBox);
		
		final JTextArea tAreaSinonime = new JTextArea();
		tAreaSinonime.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
			char c = e.getKeyChar();
					if(Character.isDigit(c))
					{
						e.consume();
				}}});
		tAreaSinonime.setFont(new Font("Constantia", Font.PLAIN, 21));
		tAreaSinonime.setBackground(SystemColor.scrollbar);
		tAreaSinonime.setBounds(297, 276, 454, 56);
		p.add(tAreaSinonime);
		
		JButton bSave = new JButton("Salveaza cuvant");
		bSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			
				String denumire=tDenumire.getText();
				String tip=comboBox.getSelectedItem().toString();
				String explicatie=tAreaExplicatie.getText();
				String sinonime=tAreaSinonime.getText();
			
				if(verificaCampurileAdaugare(denumire,explicatie)==true && verificaListadeSinonime(sinonime)==true
						&& verificaDenumirea(denumire)==true)
				{
					DetaliiCuvant info=new DetaliiCuvant();
					info.setExplicatie(explicatie);
					info.setTip(tip);
					info.setListaSinonime(sinonime);
					info.setFavorite("NU");
					
					boolean bool=true;
					ArrayList<String> sin=DICTIONAR.returneazaCuvinteleSortate();
					ArrayList<String> sinonimeA=DICTIONAR.convertStringSinonime(sinonime);
					
					for(String a:sinonimeA)
					{
						if(DICTIONAR.cautaCuvant(a)==false && a.equals("")==false)
						{
							bool=false;
							JOptionPane.showMessageDialog(null,
									"Deoarece sinonimul '"+a+"' nu exista in dictionar"+ 
											" nu il puteti adauga la lista de sinonime a acestui cuvant!","",JOptionPane.INFORMATION_MESSAGE);	
						}
					}
					
					if(bool==true)
					{
					DICTIONAR.adaugaCuvant(denumire, info);
					//DICTIONAR.actualizeazaSinonimeleDupaAdaugare(denumire,info.getListaSinonime());

					JOptionPane.showMessageDialog(null,
							"Cuvantul a fost adaugat!","",JOptionPane.INFORMATION_MESSAGE);	
					getThisPanel().removeAll();
					getThisPanel().revalidate();
					getThisPanel().add(vizualizeazaCuvintele(addJTableDenumireCuvinte()));
					getThisPanel().repaint();
					getThisPanel().revalidate();
					
					}
				}
			}
		});
		bSave.setBorder(new LineBorder(new Color(17,78,96), 4, true));
		bSave.setBackground(Color.LIGHT_GRAY);
		bSave.setForeground(new Color(17,78,96));
		bSave.setFont(new Font("Tempus Sans ITC", Font.BOLD, 22));
		bSave.setBounds(414, 354, 199, 40);
		p.add(bSave);
	
		return p;
	}
	
	protected JPanel veziDetaliiPanel(final String denumire,final DetaliiCuvant info)
	{
		final JPanel p=new JPanel();
		p.setBackground(new Color(225,226,227));
		p.setLayout(null);
			
		final JLabel lblDenumire = new JLabel("Denumire:");
		lblDenumire.setBounds(140, 60, 118, 26);
		lblDenumire.setBackground(Color.red);
		lblDenumire.setForeground(new Color(17,78,96));
		lblDenumire.setFont(new Font("Tempus Sans ITC", Font.BOLD, 25));
		p.add(lblDenumire);
		
		JLabel lblExplicatie = new JLabel("Explicatie:");
		lblExplicatie.setBounds(140, 167, 118, 26);
		lblExplicatie.setForeground(new Color(17,78,96));
		lblExplicatie.setFont(new Font("Tempus Sans ITC", Font.BOLD, 25));
		p.add(lblExplicatie);
	
		final JTextArea tAExplicatie = new JTextArea(info.getExplicatie());
		tAExplicatie.setBounds(268, 167, 454, 137);
		tAExplicatie.setEditable(false);
		tAExplicatie.setBackground(new Color(225,226,227));
		tAExplicatie.setFont(new Font("Constantia", Font.PLAIN, 22));
		p.add(tAExplicatie);
		
		JLabel lblTip = new JLabel("Tip:");
		lblTip.setBounds(215, 116, 45, 26);
		lblTip.setForeground(new Color(17,78,96));
		lblTip.setFont(new Font("Tempus Sans ITC", Font.BOLD, 25));
		p.add(lblTip);
		
		JLabel lbDenumire = new JLabel(denumire);
		lbDenumire.setBounds(268, 60, 454, 30);
		lbDenumire.setFont(new Font("Constantia", Font.BOLD, 24));
		p.add(lbDenumire);
		
		JLabel lbTip = new JLabel(info.getTip());
		lbTip.setBounds(268, 116, 454, 30);
		lbTip.setFont(new Font("Constantia", Font.PLAIN, 24));
		p.add(lbTip);
		
		JLabel lblSinonime = new JLabel("Sinonime:");
		lblSinonime.setBounds(136, 316, 122, 26);
		lblSinonime.setForeground(new Color(17,78,96));
		lblSinonime.setFont(new Font("Tempus Sans ITC", Font.BOLD, 27));
		p.add(lblSinonime);
		
		final JPanel pCurentSinonime = new JPanel();
		pCurentSinonime.setBounds(268, 315, 454, 82);
		pCurentSinonime.setBackground(new Color(225, 226, 227));
		p.add(pCurentSinonime);
		pCurentSinonime.setLayout(new CardLayout(0, 0));
		
		//=====================================================================
		final JPanel pLabeluriSinonime = new JPanel();
		pLabeluriSinonime.setLayout(null);
		pLabeluriSinonime.setBackground(new Color(225, 226, 227));
		pCurentSinonime.add(pLabeluriSinonime);
		
		JLabel[] labelList=new JLabel[10];
		final ArrayList<String> sin=DICTIONAR.convertStringSinonime(info.getListaSinonime());
		int k=0; int j=1;
		for( int i=0;  i<sin.size(); i++)
		{
			final String denumireCurenta=sin.get(i);
			labelList[i]= new JLabel(sin.get(i)+"");
			final JLabel lCurent=labelList[i];
			lCurent.setForeground(new Color(0,97,32));
			lCurent.setFont(new Font("Constantia", Font.ITALIC, 20));
			if(i>2)
			{
				lCurent.setBounds(0+k*150, 0+j*50, 100, 30);k++;
			}
			else lCurent.setBounds(0+i*150, 0, 100, 30);
			lblDenumire.addMouseMotionListener(new MouseMotionAdapter() {
				@Override
				public void mouseMoved(MouseEvent arg0) {
					lCurent.setFont(new Font("Constantia", Font.ITALIC, 24));
					lCurent.setForeground(Color.GREEN);
				}
			});
			lCurent.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent arg0) {
					//bodyPanel.getTable().setModel(bodyPanel.addJTableDenumireCuvinte());
					DetaliiCuvant info=new DetaliiCuvant();
					info=DICTIONAR.cautaInformatiiCuvant(denumireCurenta);
					getThisPanel().removeAll();
					getThisPanel().revalidate();
					getThisPanel().add(veziDetaliiPanel(denumireCurenta, info));
					getThisPanel().repaint();
					getThisPanel().revalidate();
					
			}});
			pLabeluriSinonime.add(lCurent);
		}
		
		JLabel adaugaSinonime = new JLabel("");
		adaugaSinonime.setBounds(215, 340, 33, 30);
		adaugaSinonime.setBackground(new Color(50, 205, 50));
		adaugaSinonime.setForeground(new Color(0, 128, 0));
		adaugaSinonime.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				pLabeluriSinonime.removeAll();
				pLabeluriSinonime.revalidate();
				pLabeluriSinonime.repaint();

				final JTextField textField = new JTextField();
				textField.addKeyListener(new KeyAdapter() {
					@Override
					public void keyTyped(KeyEvent e) {
					char c = e.getKeyChar();
							if(Character.isDigit(c))
							{
								e.consume();
						}}});
				textField.setBounds(0, 0, 250, 30);
				pLabeluriSinonime.add(textField);
				textField.setFont(new Font("Constantia", Font.PLAIN, 19));
				textField.setBackground(Color.LIGHT_GRAY);
				textField.setColumns(10);
				
				 
				JLabel label_1 = new JLabel("");
				label_1.addMouseListener(new MouseAdapter() {
					@Override
					public void mouseClicked(MouseEvent e) {
					
						if(textField.getText().length()==0)
						{
							beeb=false;
							getThisPanel().removeAll();
							getThisPanel().revalidate();
							getThisPanel().add(veziDetaliiPanel(denumire,info));
							getThisPanel().repaint();
							getThisPanel().revalidate();
						}
				else{
						if(Character.isAlphabetic( textField.getText().charAt(0))==false
								||DICTIONAR.cautaCuvant(textField.getText())==false 
								||textField.getText().equals(denumire)==true 
								|| info.getListaSinonime().contains(textField.getText())==true)
						{
							beeb=false;
							JOptionPane.showMessageDialog(null,
									"-->Acest cuvant nu poate fi validat!","",JOptionPane.ERROR_MESSAGE);
						}
				
						if(beeb==true )
						{
							if(DICTIONAR.getCUVINTE().get(denumire).getListaSinonime().length()==0)
							{
					
								JOptionPane.showMessageDialog(null,
										"-->length 0!","",JOptionPane.ERROR_MESSAGE);
							  DICTIONAR.adaugaSinonim(denumire,textField.getText());
							  getThisPanel().removeAll();
							  getThisPanel().revalidate();
							  getThisPanel().add(veziDetaliiPanel(denumire,DICTIONAR.getCUVINTE().get(denumire)));
							  getThisPanel().repaint();
							  getThisPanel().revalidate();
							}
							else { 
								JOptionPane.showMessageDialog(null,
										"-->2222!","",JOptionPane.ERROR_MESSAGE);
							 DICTIONAR.adaugaSinonim(denumire,DICTIONAR.getCUVINTE().get(denumire).getListaSinonime()+","+textField.getText());
							
							table.setModel(addJTableDenumireCuvinte());
							getThisPanel().removeAll();
							getThisPanel().revalidate();
							getThisPanel().add(veziDetaliiPanel(denumire,DICTIONAR.getCUVINTE().get(denumire)));
							getThisPanel().repaint();
							getThisPanel().revalidate();}
							
						
						}}}});
				label_1.setBounds(260, 0,70, 30);
				pLabeluriSinonime.add(label_1);
				pCurentSinonime.setBackground(new Color(225,226,227));
				label_1.setIcon(new ImageIcon("src\\main\\resources\\ok.png"));
				
			}
			
		});
		adaugaSinonime.setIcon(new ImageIcon("src\\main\\resources\\add.png"));
		p.add(adaugaSinonime);
		
		JLabel lblAdauga_1 = new JLabel("Adauga");
		lblAdauga_1.setBounds(150, 340, 72, 26);
		lblAdauga_1.setForeground(Color.GRAY);
		lblAdauga_1.setFont(new Font("Tempus Sans ITC", Font.BOLD, 17));
		p.add(lblAdauga_1);
		
		JLabel sterge = new JLabel("");
		sterge.setBounds(875, 24, 59, 64);
		sterge.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
			 int raspuns=JOptionPane.showConfirmDialog( null, "Sunteti sigur ca doriti sa stergeti acest cuvant?"
						,"Confirmare",JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE);
			
			 if(raspuns==JOptionPane.YES_OPTION )
			 {
				DICTIONAR.stergeCuvant(denumire, info);
				JOptionPane.showMessageDialog(null,
						"Cuvantul a fost sters!","",JOptionPane.INFORMATION_MESSAGE);	
				
				getTable().setModel(addJTableDenumireCuvinte());
				getThisPanel().removeAll();
				getThisPanel().revalidate();
				getThisPanel().add(vizualizeazaCuvintele(addJTableDenumireCuvinte()));
				getThisPanel().repaint();
				getThisPanel().revalidate();
			}
			}
		});
		sterge.setIcon(new ImageIcon("src\\main\\resources\\delete.png"));
		p.add(sterge);
		
		
		////////////////////////FAVORITE//////////////////////////////////////////////////////////
		if(DICTIONAR.getCUVINTE().get(denumire).getFavorite().equals("DA"))
		{
			
		JLabel lStea = new JLabel("");
		lStea.setIcon(new ImageIcon("src\\main\\resources\\stea.png"));
		lStea.setBounds(880, 99, 59, 47);
		p.add(lStea);
		
		}
		else
		{
			

			final JLabel label_2 = new JLabel("");
			label_2.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent arg0) {
					
						
						System.out.println("STEP FIANL");
						DICTIONAR.getCUVINTE().get(denumire).setFavorite("DA");
						info.setFavorite("DA");
						table.setModel(addJTableDenumireCuvinte());
						JOptionPane.showMessageDialog(null,
								"Cuvantul a fost adaugat la  'Favorit'!!","",JOptionPane.INFORMATION_MESSAGE);	
						
						getThisPanel().removeAll();
						getThisPanel().revalidate();
						getThisPanel().add(veziDetaliiPanel(denumire,DICTIONAR.cautaInformatiiCuvant(denumire)));
						getThisPanel().repaint();
						getThisPanel().revalidate();
				}
			});
			label_2.setIcon(new ImageIcon("src\\main\\resources\\adFavorite.png"));
			label_2.setBounds(880, 99, 59, 47);
			p.add(label_2);
		}
		///=====================================================================
		return p;
	}
	
	protected JPanel vizualizeazaCuvinteleFavorite()
	{
		JPanel p=new JPanel();
		p.setBackground(new Color(225,226,227));
		p.setLayout(null);
		
		if(DICTIONAR.returneazaCuvinteFavorite().size()==0)
		{
			final JTextArea tAreaMessaje = new JTextArea("Nu exista cuvinte favorite!!");
			tAreaMessaje.setForeground(new Color(17,78,96));
			tAreaMessaje.setBackground(new Color(225,226,227));
			tAreaMessaje.setFont(new Font("Tempus Sans ITC", Font.BOLD, 30));
			tAreaMessaje.setBounds(280, 147, 650, 350);
			p.add(tAreaMessaje);
		
		}
		else
		{
			JScrollPane scrollPane1 = new JScrollPane();
			scrollPane1.setBounds(0, 0, 959, 434);
			scrollPane1.getViewport().setBackground(new Color(225,226,227));
			p.add(scrollPane1);
			scrollPane1.getVerticalScrollBar().setBackground(new Color(17,78,96));
			JTable table1 = new JTable();
			table1.setModel(addJTableCuvinteFavorite());
			table1.setFont(new Font("Trebuchet MS", Font.PLAIN, 24));
			table1.setRowHeight(45);
			table1.setForeground(new Color(70, 70, 70));
			table1.setBackground(new Color(225,226,227));
			table1.getTableHeader().setPreferredSize(new Dimension(0,0));
			DefaultTableCellRenderer rightRenderer1 = new DefaultTableCellRenderer();
			rightRenderer1.setHorizontalAlignment(DefaultTableCellRenderer.CENTER);
			table1.getColumn("").setCellRenderer( rightRenderer1 );
			scrollPane1.setViewportView(table1);
		}
		return p;
	}
	
	protected JPanel setMode()
	{
		JPanel panel = new JPanel();
		panel.setBackground(new Color(225,226,227));
		panel.setBounds(706, 75, 258, 171);
		panel.setLayout(null);
		
		rdbtnStartWith = new JRadioButton("Incepe cu ?");
		rdbtnStartWith.setFont(new Font("Tahoma", Font.PLAIN, 17));
		rdbtnStartWith.setBounds(370, 30,150, 50);
		panel.add(rdbtnStartWith);
		
		 rdbtnSfarseste = new JRadioButton("Se termina cu ?");
		rdbtnSfarseste.setFont(new Font("Tahoma", Font.PLAIN, 17));
		rdbtnSfarseste.setBounds(370, 90, 150, 50);
		panel.add(rdbtnSfarseste);
		
		 rdbtnExpresie = new JRadioButton("Expresie");
		rdbtnExpresie.setFont(new Font("Tahoma", Font.PLAIN, 17));
		rdbtnExpresie.setBounds(370, 150, 150, 50);
		panel.add(rdbtnExpresie);
	
		 ButtonGroup group = new ButtonGroup();
		    group.add(rdbtnStartWith);
		    group.add(rdbtnSfarseste);
		    group.add(rdbtnExpresie);
		  
		
		JButton btnNewButton = new JButton("OK");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				if(rdbtnStartWith.isSelected()==true)
					    {
					    	radioB=rdbtnStartWith.getText();
					    }
					    if(rdbtnSfarseste.isSelected()==true)
					    {
					    	radioB=rdbtnSfarseste.getText();
					    }
					    if(rdbtnExpresie.isSelected()==true)
					    {
					    	radioB=rdbtnExpresie.getText();
					    }
						getThisPanel().removeAll();
						getThisPanel().revalidate();
						getThisPanel().add(vizualizeazaCuvintele(addJTableDenumireCuvinte()));
						getThisPanel().repaint();
						getThisPanel().revalidate();
					    
					    
			}});
		btnNewButton.setFont(new Font("Tahoma", Font.PLAIN, 17));
		btnNewButton.setBounds(370, 215, 90, 29);
		panel.add(btnNewButton);
		
		return panel;
	}
	
	protected TableModel cautaCuvinte(String prefix)
	{
		DefaultTableModel model1=new DefaultTableModel();
		model1.addColumn("");
		ArrayList<String> sss=new ArrayList<String>();
		if(radioB!=null){
		if(radioB.equals(rdbtnStartWith.getText())==true)
		{
			 sss=DICTIONAR.searchForStartWith(prefix);
		}
		if(radioB.equals(rdbtnSfarseste.getText())==true)
		{
			 sss=DICTIONAR.searchStopWith(prefix);
		}
		if(radioB.equals(rdbtnExpresie.getText())==true)
		{
			for(String ss:DICTIONAR.returneazaCuvinteleSortate()) 
			{
				if (DICTIONAR.matches(ss, prefix)==true) 
				{
					sss.add(ss);
				}
			}
		}
		if(sss!=null)
		{
				for(int i=0; i< sss.size(); i++)
				{
					model1.addRow(new Object[]{sss.get(i)});
				}
		}}
		return model1;
	}
	
	protected TableModel addJTableCuvinteFavorite()
	{
		DefaultTableModel model1=new DefaultTableModel();
		model1.addColumn("");
		ArrayList<String> sss=DICTIONAR.returneazaCuvinteFavorite();
		if(sss!=null)
		{
				for(int i=0; i< sss.size(); i++)
				{
					model1.addRow(new Object[]{sss.get(i)});
				}
		}
		return model1;
	}
	
	protected boolean verificaCampurileAdaugare(String denumire,String explicatie)
	{
		if(denumire.length()==0)
		{
			JOptionPane.showMessageDialog(null,
					"Introduceti denumirea cuvantului obligatoriu!","",JOptionPane.INFORMATION_MESSAGE);	
			return false;
		}
		if(explicatie.length()==0)
		{
			JOptionPane.showMessageDialog(null,
					"Introduceti explicatia cuvantului obligatoriu!","",JOptionPane.INFORMATION_MESSAGE);	
			return false;
		}
		
		return true;
	}

	protected JPanel getThisPanel()
	{
		return this;
	}
	
	public boolean verificaListadeSinonime(String sinonime)
	{
		 int sz = sinonime.length();
	      for (int i = 0; i < sz; i++) {
	          if ((Character.isLetter(sinonime.charAt(i)) == false) && (sinonime.charAt(i) != '-') 
	        		  && (sinonime.charAt(i) != ',')) {
	        	  JOptionPane.showMessageDialog(null,
		  					"Introduceti sinonimele separate prin virgula!","",JOptionPane.ERROR_MESSAGE);	
	              return false;
	          }
	      }
	      return true;
	}
	
	public boolean verificaDenumirea(String denumire)
	{
		char c = denumire.charAt(0);
		if (Character.isAlphabetic(c)==true) 
		{ 
			return true;
		}
		JOptionPane.showMessageDialog(null,
					"Denumirea trebuie sa inceapa cu o litera!","",JOptionPane.ERROR_MESSAGE);	
		return false;
	}
	
	protected void  changeDictioanry(Dictionar DICTIONAR)
	{
		this.DICTIONAR=DICTIONAR;
	}
}
