package gui;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.ParseException;
import model.Dictionar;

public interface Fisier 
{
		public Dictionar loadInformation(String fileName,Dictionar DICTIONAR) throws FileNotFoundException, IOException, ParseException, org.json.simple.parser.ParseException;
		public void writeInformation(String fileName,Dictionar DICTIONAR) throws IOException; 
}
