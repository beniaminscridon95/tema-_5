package gui;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;

import model.DetaliiCuvant;
import model.Dictionar;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class FisierJSon implements Fisier
{
	public Dictionar loadInformation(String fileName, Dictionar DICTIONAR) throws FileNotFoundException,IOException, ParseException, org.json.simple.parser.ParseException 
	{
		
		String path="src/main/resources/"+fileName;
		JSONParser parser = new JSONParser();
		JSONArray a= (JSONArray) parser.parse(new FileReader(path));
		JSONArray cuvant = (JSONArray) a;
		for(Object o : cuvant) 
		{
			JSONObject c = (JSONObject) o;
			String denumire=(String) c.get("DENUMIRE");
			String tip = (String) c.get("TIP");
			String explicatie = (String) c.get("EXPLICATIE");
			String sinonime = (String) c.get("SINONIME");
			String favorit=(String) c.get("FAVORIT");
			DetaliiCuvant info=new DetaliiCuvant();
			info.setExplicatie(explicatie);
			info.setListaSinonime(sinonime);
			info.setTip(tip);		
			info.setFavorite(favorit);
			
			DICTIONAR.getCUVINTE().put(denumire,info);
		 }
		return DICTIONAR;
}

	@SuppressWarnings("unchecked")
	public void writeInformation(String fileName, Dictionar DICTIONAR) throws IOException
	 {
		String path="src/main/resources/"+fileName;
		ArrayList<String> cuvintele=DICTIONAR.returneazaCuvinteleSortate();
		FileWriter file = new FileWriter(path);
		JSONArray ar=new JSONArray();
		for(String s:cuvintele)
		{
			JSONObject obj = new JSONObject();
			obj.put("DENUMIRE",s);
			obj.put("EXPLICATIE",DICTIONAR.getCUVINTE().get(s).getExplicatie());
			obj.put("TIP",DICTIONAR.getCUVINTE().get(s).getTip());
			obj.put("SINONIME",DICTIONAR.getCUVINTE().get(s).getListaSinonime());
			obj.put("FAVORIT",DICTIONAR.getCUVINTE().get(s).getFavorite());
			ar.add(obj);
		}
		try
		{	
			file.write(ar.toString());
			file.flush();
			file.close();
		}
		catch (IOException e) {
				e.printStackTrace();
		}
	}
}