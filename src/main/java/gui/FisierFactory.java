package gui;

public class FisierFactory
{
	public static Fisier getInformation(String fileName)
	  {
	    if ( fileName.contains(".ser")==true )
	      return new FisierSerializabil();
	    else if ( fileName.contains(".json")==true)
	      return new FisierJSon();
	  return null;
	  }
}