package gui;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.ParseException;
import javax.swing.JFileChooser;
import model.Dictionar;
////open /save 
////debugui
public class OpenAndSaveFile 
{

	protected static void saveFile(Dictionar DICTIONAR)
	{
		JFileChooser fileChooser = new JFileChooser(new File("src\\main\\resources\\"));
		fileChooser.setDialogTitle("Specify a file to save");   
		fileChooser.setFileFilter(new FileTypeFilter(".ser","Serializabil"));
		fileChooser.setFileFilter(new FileTypeFilter(".json","JSON"));
		int userSelection = fileChooser.showSaveDialog(null);
	 
		if (userSelection == JFileChooser.APPROVE_OPTION) 
		{
			File fileToSave = fileChooser.getSelectedFile();
			String nameFile=fileToSave.getName();
			Fisier fact=FisierFactory.getInformation(nameFile);
			try 
			{
				fact.writeInformation(nameFile,DICTIONAR);
			}
			catch(Exception exc){
				exc.printStackTrace();
			}
		}
	}
	
	protected static Dictionar fileOpenChooser()
	{
		Dictionar DICTIONAR=new Dictionar();
		JFileChooser fileChooser = new JFileChooser(new File("src\\main\\resources\\"));
		fileChooser.setDialogTitle("Alegeti un fisier ce contine cuvinte");
		fileChooser.setFileFilter(new FileTypeFilter(".ser","Serializabil"));
		fileChooser.setFileFilter(new FileTypeFilter(".json","JSON"));
		int returnValue = fileChooser.showOpenDialog(null);
		if (returnValue == JFileChooser.APPROVE_OPTION) 
		{
			File selectedFile = fileChooser.getSelectedFile();
			Fisier fact=FisierFactory.getInformation(selectedFile.getName());
			try 
			{
				DICTIONAR=fact.loadInformation(selectedFile.getName(),DICTIONAR);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (ParseException e) {
				e.printStackTrace();
			} catch (org.json.simple.parser.ParseException e) {
				e.printStackTrace();
			}
	
		}
		return DICTIONAR;
	}	
}
