package gui;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.ParseException;

import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import model.DetaliiCuvant;
import model.Dictionar;

public class FooterPanel extends JPanel 
{
	private static final long serialVersionUID = 1L;
	private Dictionar DICTIONAR;
	private BodyPanel body; 
	protected JLabel bImport;
	
	
	public FooterPanel(Dictionar d,BodyPanel body) 
	{
		DICTIONAR=d;
		this.body=body;
		setBackground(new Color(12, 78, 96));
		setLayout(null);
		adaugaButoanele();
		adaugaLabelurile();
	}
	
	protected void adaugaButoanele()
	{
		JLabel bDetalii = new JLabel("");
		bDetalii.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
			if(DICTIONAR.getCUVINTE().size()!=0)
			{
				int row=body.getTable().getSelectedRow();
				if(row==-1)
				{
					JOptionPane.showMessageDialog(null,	"Va rugam selectati un cuvant din dictionar! "+
							"Intrati la sectiunea 'Cuvinte' pentru a vizualiza cuvintele!","",JOptionPane.INFORMATION_MESSAGE);	
				}
				else
				{
					String denumire=body.getTable().getModel().getValueAt(row,0).toString();
					DetaliiCuvant info=new DetaliiCuvant();
					info=DICTIONAR.cautaInformatiiCuvant(denumire);
					
					body.removeAll();
					body.revalidate();
					body.getTable().setModel(body.addJTableDenumireCuvinte());
					body.add(body.veziDetaliiPanel(denumire,info));
					body.repaint();
					body.revalidate();
				}}
			else
			{
				JOptionPane.showMessageDialog(null,	"Va rugam selectati un cuvant din dictionar! "+
						"Intrati la sectiunea 'Cuvinte' pentru a vizualiza cuvintele!","",JOptionPane.INFORMATION_MESSAGE);	
			
			}}});
		bDetalii.setBounds(517, 25, 69, 68);
		this.add(bDetalii);
		bDetalii.setIcon(new ImageIcon("src\\main\\resources\\details.png"));
		
		JLabel bHome = new JLabel("");
		bHome.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				body.removeAll();
				body.revalidate();
				body.add(body.adaugaPanelWelcome());
				body.repaint();
				body.revalidate();
			}
		});
		bHome.setIcon(new ImageIcon("src\\main\\resources\\home.png"));
		bHome.setBounds(81, 25, 83, 68);
		this.add(bHome);
		
		bImport = new JLabel("");
		bImport.setIcon(new ImageIcon("src\\main\\resources\\import.png"));
		bImport.setBounds(225, 25, 77, 68);
		this.add(bImport);
		
		JLabel bFavorit = new JLabel("");
		bFavorit.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				body.removeAll();
				body.revalidate();
				body.add(body.vizualizeazaCuvinteleFavorite());
				body.repaint();
				body.revalidate();
			}
		});
		bFavorit.setIcon(new ImageIcon("src\\main\\resources\\favorite.png"));
		bFavorit.setBounds(807, 25, 77, 64);
		this.add(bFavorit);
		
		JLabel bAdauga = new JLabel("");
		bAdauga.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				body.removeAll();
				body.revalidate();
				body.add(body.adaugaCuvantPanel());
				body.repaint();
				body.revalidate();
			}
		});
		bAdauga.setIcon(new ImageIcon("src\\main\\resources\\add word.png"));
		bAdauga.setBounds(665, 25, 61, 70);
		this.add(bAdauga);
	
		JLabel bTabel = new JLabel("");
		bTabel.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				body.removeAll();
				body.revalidate();
				body.add(body.vizualizeazaCuvintele(body.addJTableDenumireCuvinte()));
				body.repaint();
				body.revalidate();
			}
		});
		bTabel.setIcon(new ImageIcon("src\\main\\resources\\search.png"));
		bTabel.setBounds(377, 25, 77, 81);
		this.add(bTabel);
	}
	
	public void adaugaLabelurile()
	{
		JLabel lblHome1 = new JLabel("Home");
		lblHome1.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblHome1.setForeground(new Color(245, 255, 250));
		lblHome1.setBounds(94, 101, 50, 22);
		this.add(lblHome1);
		
		JLabel lblImporteazaDictioanr1 = new JLabel("Resurse");
		lblImporteazaDictioanr1.setForeground(new Color(245, 255, 250));
		lblImporteazaDictioanr1.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblImporteazaDictioanr1.setBounds(235, 101, 69, 22);
		this.add(lblImporteazaDictioanr1);
		
		JLabel lblDetalii1 = new JLabel("Detalii cuvant");
		lblDetalii1.setForeground(new Color(245, 255, 250));
		lblDetalii1.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblDetalii1.setBounds(498, 101, 106, 22);
		this.add(lblDetalii1);
		
		JLabel lblAdauga1 = new JLabel("Adauga cuvant");
		lblAdauga1.setForeground(new Color(245, 255, 250));
		lblAdauga1.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblAdauga1.setBounds(648, 101, 117, 22);
		this.add(lblAdauga1);
		
		JLabel lblFavorit1 = new JLabel("Favorit");
		lblFavorit1.setForeground(new Color(245, 255, 250));
		lblFavorit1.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblFavorit1.setBounds(817, 100, 61, 22);
		this.add(lblFavorit1);
		
		JLabel lblVizualizeaza1 = new JLabel("Cuvinte");
		lblVizualizeaza1.setForeground(new Color(245, 255, 250));
		lblVizualizeaza1.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblVizualizeaza1.setBounds(377, 101, 69, 22);
		this.add(lblVizualizeaza1);
	}

	protected void  changeDictioanry(Dictionar DICTIONAR)
	{
		this.DICTIONAR=DICTIONAR;
	}
}
