package gui;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.ParseException;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JPanel;




import model.Dictionar;

public class MainPanel 
{
	private static JFrame frmDictionaryapp;
	private JPanel mainPanelHeader;
	private JPanel mainPanelBody;
	private JPanel mainPanelFooter;
	private static Dictionar DICTIONAR;

	public MainPanel() 
	{
		DICTIONAR=OpenAndSaveFile.fileOpenChooser();
		
		initialize(); 
		
		final BodyPanel body=new BodyPanel(DICTIONAR);
		mainPanelBody.add(body,"body");
	
		HeaderPanel header=new HeaderPanel(body);
		mainPanelHeader.add(header,"header");
		
		final FooterPanel footer=new FooterPanel(DICTIONAR,body);
		mainPanelFooter.add(footer, "footer");
		
		footer.bImport.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				DICTIONAR=OpenAndSaveFile.fileOpenChooser();
				body.changeDictioanry(DICTIONAR);
				footer.changeDictioanry(DICTIONAR);
			}
		}); 
		
	}

	private void initialize() 
	{
		frmDictionaryapp = new JFrame();
		frmDictionaryapp.setResizable(false);
		frmDictionaryapp.getContentPane().setBackground(new Color(0, 0, 0));
		frmDictionaryapp.setTitle("DictionaryApp");
		frmDictionaryapp.setBounds(100, 100, 965, 670);
		frmDictionaryapp.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmDictionaryapp.addWindowListener(new java.awt.event.WindowAdapter() {
		    @Override
		    public void windowClosing(java.awt.event.WindowEvent windowEvent) {
		    	OpenAndSaveFile.saveFile(DICTIONAR);
		    }});
		frmDictionaryapp.getContentPane().setLayout(null);
	
		mainPanelHeader = new JPanel();
		mainPanelHeader.setBounds(0, 0, 959, 87);
		frmDictionaryapp.getContentPane().add(mainPanelHeader);
		mainPanelHeader.setLayout(new CardLayout(0, 0));
		
		mainPanelBody = new JPanel();
		mainPanelBody.setBounds(0, 87, 959, 420);
		frmDictionaryapp.getContentPane().add(mainPanelBody);
		mainPanelBody.setLayout(new CardLayout(0, 0));
		
		mainPanelFooter = new JPanel();
		mainPanelFooter.setBounds(0, 507, 959, 134);
		frmDictionaryapp.getContentPane().add(mainPanelFooter);
		mainPanelFooter.setLayout(new CardLayout(0, 0));
	}
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainPanel window = new MainPanel();
					window.frmDictionaryapp.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
}
