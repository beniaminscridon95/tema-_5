package gui;
import java.awt.Color;
import java.awt.Font;
import java.awt.SystemColor;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Calendar;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import model.Dictionar;
import javax.swing.JRadioButton;
import javax.swing.JButton;

public class HeaderPanel extends JPanel 
{
	private static final long serialVersionUID = 1L;
	private BodyPanel body;
	private JTextField tSearch;
	public HeaderPanel(BodyPanel  body) 
	{
		this.body=body;
		this.setBackground(new Color(12, 78, 96));
		adaugaJLabelurile();
		adaugaJTextFieldurile();
		
	}
		
	protected void adaugaJLabelurile()
	{
		int ziua=Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
		int luna=Calendar.getInstance().get(Calendar.MONTH);
		int an=Calendar.getInstance().get(Calendar.YEAR);
		setLayout(null);
		JLabel lbData = new JLabel(ziua+"/"+luna+"/"+an);
		lbData.setBounds(875, 11, 74, 14);
		lbData.setFont(new Font("Trebuchet MS", Font.PLAIN, 15));
		lbData.setForeground(new Color(255, 255, 255));
		this.add(lbData);
		
		JLabel lbLoupeSearch = new JLabel("");
		lbLoupeSearch.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				
				if(body.radioB!=null)
				{
				String s=tSearch.getText();
				body.getTable().setModel(body.cautaCuvinte(s));
				body.removeAll();
				body.revalidate();
				body.add(body.vizualizeazaCuvintele(body.cautaCuvinte(s)));
				body.repaint();
				body.revalidate();
				}
			}
		});
		lbLoupeSearch.setBounds(764, 30, 40, 34);
		lbLoupeSearch.setIcon(new ImageIcon("src\\main\\resources\\loupe.jpg"));
		this.add(lbLoupeSearch);
		
		JLabel lbLOGO = new JLabel("");
		lbLOGO.setBounds(35, 11, 192, 57);
		lbLOGO.setIcon(new ImageIcon("src\\main\\resources\\logo.png"));
		this.add(lbLOGO);
	}
		
	protected void adaugaJTextFieldurile()
	{
		tSearch = new JTextField("                  cauta cuvant");
		tSearch.setBounds(298, 30, 406, 34);
		tSearch.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				tSearch.setText("");
				tSearch.setBackground(new Color(255,255, 255));
				tSearch.setForeground(new Color(7, 38, 51));
			}});
		tSearch.setForeground(SystemColor.scrollbar);
		tSearch.setFont(new Font("Trebuchet MS", Font.PLAIN, 22));
		this.add(tSearch);
		tSearch.setColumns(10);
		
		
		
		JLabel lbSettings = new JLabel("");
		lbSettings.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				
				body.removeAll();
				body.revalidate();
				body.add(body.setMode());
				body.repaint();
				body.revalidate();
				
				
			}
		});
		lbSettings.setBounds(714, 30, 40, 32);
		lbSettings.setIcon(new ImageIcon("src\\main\\resources\\sett.png"));
		add(lbSettings);
	}
}