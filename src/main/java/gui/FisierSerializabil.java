package gui;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import model.Dictionar;

public class FisierSerializabil implements Fisier 
{
	public Dictionar loadInformation(String fileName,Dictionar DICTIONAR) 
	{
		
		try
		{
		     FileInputStream fileIn = new FileInputStream("src/main/resources/"+fileName);
		     ObjectInputStream in = new ObjectInputStream(fileIn);
		     DICTIONAR =(Dictionar) in.readObject();
		         in.close();
		         fileIn.close();
		      }catch(IOException i)
		      {
		         i.printStackTrace();
		         return null;
		      }catch(ClassNotFoundException c)
		      {
		         c.printStackTrace();
		         return null;
		      }
		return DICTIONAR;
	}

	public void writeInformation(String fileName,Dictionar DICTIONAR) 
	{
		 try
	     {
	        FileOutputStream fileOut=new FileOutputStream("src/main/resources/"+fileName);;
	        ObjectOutputStream out = new ObjectOutputStream(fileOut);
	        out.writeObject(DICTIONAR);
	        out.close();
	        fileOut.close();

	     }catch(IOException i){
	         i.printStackTrace();
	     }
	}
}
